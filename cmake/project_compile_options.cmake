function(project_compile_options)
    if(MSVC)
        target_compile_options(${PROJECT_NAME} PRIVATE
            /wd4251
            /wd4820
            /wd4355

            # STL's packaged_task really does not like having warnings treated as errors turned on...
            /wd5204
            /wd4625
            /wd4514
            /wd5220
            /wd4626
            /wd5026
            /wd5027
            /wd4710
            /wd4711
        )
    endif()
endfunction()