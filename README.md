# reverse

Reverse is a C++20 approach to parallelized file parsing and line reversing.

## Requirements

* CMake: v3.22+
* GCC: v12+
* Clang: v16+
* MSVC: v2022+

## Build

See [CMakePresets](./CMakePresets.json) for build configurations.
See [.gitlab-ci.yml](./.gitlab-ci.yml) for usage in build environments.

### GCC

```shell
> cmake --preset gcc-release
> cmake --build --preset gcc-release
```

### Clang

```shell
> cmake --preset clang-release
> cmake --build --preset clang-release
```
