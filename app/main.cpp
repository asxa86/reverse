#include <reverse/util/ThreadPool.h>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <ranges>

int main(int argc, char** argv)
{
	std::filesystem::path file;

	// Default to the configuration defined input file if a file isn't given.
	switch(argc)
	{
		case 1:
			file = INPUT_FILE;
			break;

		case 2:
		{
			// The second argument, index 1, will contain the file path.
			constexpr auto filePos = 1;
			file = argv[filePos];
			break;
		}

		case 0:
			[[fallthrough]];
		default:
			return EXIT_FAILURE;
	}

	if(std::filesystem::exists(file) == false)
	{
		return EXIT_FAILURE;
	}

	std::mutex mutexRead;
	std::mutex mutexWrite;
	std::deque<std::string> data;
	std::condition_variable condition;
	reverse::ThreadPool pool;
	std::vector<std::future<void>> tasks;

	// Keep track of a global state to help control when to shut thread processing down cleanly.
	std::atomic<bool> processing{true};

	const auto start = std::chrono::steady_clock::now();

	// Queue a worker to parse the file on a thread.
	tasks.emplace_back(pool.enqueue(
		[file, &mutexRead, &data, &processing, &condition]
		{
			std::ifstream is{file, std::ios::in | std::ios::binary};

			if(is.is_open() == false)
			{
				processing = false;
				return;
			}

			std::string line;

			while(std::getline(is, line) && processing == true)
			{
				std::unique_lock lock(mutexRead);
				data.emplace_back(std::move(line));
			}

			// Block until data is empty. This will prevent processing from being turned off until we are truly done processing all of the data.
			std::unique_lock lock(mutexRead);
			condition.wait(lock, [&data] { return std::empty(data) == true; });

			// We have finished loading the file. Shutdown processing.
			processing = false;
		}));

	std::ofstream os{"output.txt", std::ios::out | std::ios::binary};

	if(os.is_open() == false)
	{
		processing = false;
		return EXIT_FAILURE;
	}

	// Continue queueing workers until the file is done processing.
	while(processing == true)
	{
		std::string line;

		{
			std::unique_lock lock(mutexRead);

			if(std::empty(data) == true)
			{
				continue;
			}

			line = std::move(data.front());
			data.pop_front();
		}

		// Signal the parsing thread to turn off processing if we've processed all of the data.
		condition.notify_one();

		tasks.emplace_back(pool.enqueue(
			[line = std::move(line), &os, &mutexWrite]() mutable
			{
				const auto reversed = line | std::views::reverse;

				std::unique_lock lock(mutexWrite);

				for(auto c : reversed)
				{
					os << c;
				}

				// We need to append newlines back into the file due to std::getline stripping them away.
				os << '\n';
			}));
	}

	// Wait for all tasks to complete before exiting the application.
	for(auto& task : tasks)
	{
		task.wait();
	}

	const auto elapsed = std::chrono::steady_clock::now() - start;

	// One of the tasks is the parsing thread so don't count it in the statistics.
	std::cout << "Processed " << tasks.size() - 1 << " lines in " << std::chrono::duration_cast<std::chrono::duration<double>>(elapsed).count()
			  << " seconds.\n";

	return EXIT_SUCCESS;
}
