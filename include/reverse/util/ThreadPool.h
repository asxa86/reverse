#pragma once

#include <atomic>
#include <concepts>
#include <deque>
#include <functional>
#include <future>
#include <mutex>
#include <reverse/util/export.hxx>
#include <thread>
#include <vector>

namespace reverse
{
	/// @brief A simple thread pool designed to alleviate continuous allocations of threads by keeping a configured number of threads active and
	/// available to process tasks at all time.
	class REVERSE_UTIL_EXPORT ThreadPool
	{
	public:
		/// @brief Initializes the thread pool with a given number of threads.
		/// @param x The number of threads to allocate the pool with.
		explicit ThreadPool(std::size_t x = std::thread::hardware_concurrency());

		/// @brief Exits and remaining threads and shuts down the pool.
		~ThreadPool();

		/// @brief Due to the nature of thread management, this class has disabled copying.
		ThreadPool(const ThreadPool&) = delete;

		/// @brief Due to the nature of thread management, this class has disabled copying.
		auto operator=(const ThreadPool&) -> ThreadPool& = delete;

		/// @brief Due to the nature of thread management, this class has disabled moving.
		ThreadPool(ThreadPool&&) noexcept = delete;

		/// @brief Due to the nature of thread management, this class has disabled moving.
		auto operator=(ThreadPool&&) noexcept -> ThreadPool& = delete;

		/// @brief Queue a task to be processed by the thread pool once a thread has become available.
		/// @tparam T This function only supports queing std::invocable types.
		/// @param x The invocable type to be queued by the ThreadPool.
		template <std::invocable T>
		auto enqueue(T x) -> std::future<void>
		{
			auto task = std::make_shared<std::packaged_task<void()>>(x);
			auto future = task->get_future();

			{
				std::unique_lock lock(this->mutexQueue);
				this->tasks.emplace_back([task] { (*task)(); });
			}

			// Notify worker threads a task is available.
			this->condition.notify_one();

			return future;
		}

	private:
		std::deque<std::function<void()>> tasks;
		std::vector<std::jthread> workers;
		std::mutex mutexQueue;
		std::condition_variable condition;
		std::size_t threads{};
		std::atomic<bool> running{true};
	};
}
