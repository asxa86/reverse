#include <reverse/util/ThreadPool.h>

using reverse::ThreadPool;

ThreadPool::ThreadPool(std::size_t x) : threads{x}
{
	// Kick on all threads.
	for(decltype(this->threads) i = 0; i < this->threads; i++)
	{
		this->workers.emplace_back(
			[this]
			{
				while(this->running == true)
				{
					std::function<void()> task;

					// Lock the thread and check the task queue.
					{
						std::unique_lock lock(this->mutexQueue);

						// To keep the thread from just continuously burning needless cpu cycles. Let's block the thread until a new task is available
						// or we no longer need to run.
						this->condition.wait(lock, [this] { return this->running == false || std::empty(this->tasks) == false; });

						if(this->running == false || std::empty(this->tasks) == true)
						{
							return;
						}

						task = std::move(this->tasks.front());
						this->tasks.pop_front();
					}

					task();
				}
			});
	}
}

ThreadPool::~ThreadPool()
{
	// Shuts down the threads allowing them to return.
	// jthread will clean up the threads on destruction.
	this->running = false;
	this->condition.notify_all();

	for(auto& worker : this->workers)
	{
		worker.join();
	}
}
